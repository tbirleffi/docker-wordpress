#!/bin/bash

ENV="$1";
PRJ="$2";
DIR="data/www/wordpress";
CUR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd );
COP="docker-compose.yml";
NAME="docker-wordpress.com";
source ~/.bash_profile;

runDockerComposeSetup()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [ "$ENV" != "" ]
	then
		copyFiles;
		copyDockerComposeFile;
		startServices;
		listServices;
	fi
}

listServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" ps;
	else
		docker-compose ps;
	fi
}

startServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" up -d;
	else
		docker-compose up -d;
	fi
}

copyDockerComposeFile()
{
	cp "env/$ENV/docker-compose.yml" "$COP";
}

copyFiles()
{
	if [ -d "$DIR" ]
	then
		cp "wp-config.php" "$DIR/wp-config.php";
	fi
}

runComposer()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" run composer self-update;
		docker-compose -p "$PRJ" run composer update;
	else
		docker-compose run composer self-update;
		docker-compose run composer update;
	fi
}

createFolders()
{
	mkdir -p data/logs;
}

runLocalNetworkSetup()
{
	echo Starting boot2docker if not already started;
	boot2docker status || boot2docker up;

	export the_ip=$(boot2docker ip 2>&1 | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	export old_route_ip=$(netstat -r | grep 172.17  | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");
	echo Found boot2docker ip $the_ip;

	if [ "$the_ip" == "" ]; then
		echo Could not find the ip of boot2docker.. =/;
		exit;
	fi

	echo Seting up routes. Enter sudo password;
	if [ "$old_route_ip" != "" ]; then
		sudo route -n delete -net 172.17.0.0 $old_route_ip;
	fi
	sudo route -n add -net 172.17.0.0 $the_ip;

	echo Setting up hosts;
	cp /etc/hosts hosts;
	grep -v '# boot2dockerscriptwordpress' hosts > hosts_temp;
	mv hosts_temp hosts;
	for containerid in $(docker ps -q); do
		export domain=$(docker inspect $containerid | grep '"Domainname":' | cut -f2 -d":" | cut -f2 -d'"');
		export host=$(docker inspect $containerid | grep '"Hostname":' | cut -f2 -d":" | cut -f2 -d'"');
		export ip=$(docker inspect $containerid | grep 'IPAdd' | egrep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*");

		if [[ "$domain" != "" && $domain == *"$NAME"* && "$ENV" != "jenkins" ]]; then
			env=`echo $domain| cut -d '.' -f 1`;
			cp "env/$env/$env.json" "$DIR/$domain.json";
			echo Storing values $ip $host.$domain;
			echo $ip $host $domain \# boot2dockerscriptwordpress >> hosts;
		fi
	done
	sudo mv hosts /etc/hosts;
}

createFolders;
runDockerComposeSetup;

if [ -d "$DIR" ]
then
	copyFiles;
else
	runComposer;
	copyFiles;
	startServices;
	listServices;
fi

runLocalNetworkSetup;
