#!/bin/bash

boot2docker ssh 'sudo cp "/Users/Sites/docker-wordpress/profile" "/mnt/sda1/var/lib/boot2docker/profile"';
boot2docker restart;

# Profile fix with Docker Machine
#docker-machine ssh {machine_name} 'sudo cp "/Users/Sites/docker-wordpress/profile" "/mnt/sda1/var/lib/boot2docker/profile"';
#docker-machine restart {machine_name};