<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    $_SERVER['HTTPS'] = 'on';


$config = '/' . $_SERVER['HTTP_HOST'] . '.json';
if(file_exists(dirname(__FILE__).$config))
    $config = json_decode(file_get_contents(dirname(__FILE__).$config), true);
else
    die('No config found');


define('AWS_ACCESS_KEY_ID', $config['aws-access-key'] );
define('AWS_SECRET_ACCESS_KEY', $config['aws-secret-key'] );

define('SUNRISE', 'on');
define('MULTISITE', $config['site']['multisite'] );
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', $config['site']['domain'] );
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define( 'WP_ALLOW_MULTISITE', true );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $config['database']['db_name'] );

/** MySQL database username */
define('DB_USER', $config['database']['user'] );

/** MySQL database password */
define('DB_PASSWORD', $config['database']['password'] );

/** MySQL hostname */
define('DB_HOST', $config['database']['host'] );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Nxrc@%IV#0HC2 h:ID?lR=^0l`zsXo0|dHj2Zu4&)C,jV8mH2~=r^J(`^y,e$(x`');
define('SECURE_AUTH_KEY',  'pzBd.ojK;*uXQ]1Tt7@xxqoqXb6c;BB8$*~94=3Wk#|v0m*%n-)$Vu KNkR#{nV=');
define('LOGGED_IN_KEY',    'W84Yko3C0b M)(Zq%>#Fc*XQbN|Xo*:}kI2Z5gk|IvF5W,4SzDTm7t[:L;A8RS+_');
define('NONCE_KEY',        '^DH)s#a$Q;-zn8_h5nNf<s&8E*y|zK1E3|*-CA+sD4-ovQ?$W+xg=0H6)NyfQ$>(');
define('AUTH_SALT',        'l|D|yrA*{,;mvBd^a={M:L3v^W<zT3DeFbIGxtYB`yL^J32q0*abi^U$?KpIMC`$');
define('SECURE_AUTH_SALT', 'e[!=w[%|&mXiD{k.M(&yr,N|d!9^/3{S0vJ4L!czh@%:=@6ON0(S/~QFB uUu!+/');
define('LOGGED_IN_SALT',   '4LLujf +vtdZnPB#+q+=JktJu|2b4Lgx])u{$+W+%Afw0QpR,<H1_jhE^D~h+aG@');
define('NONCE_SALT',       '0+|sh3b`!T?OKw)#>C^dO97f+tXGF]]_T,l!#+BP&l2=BjVl8Q<poyHEVQh[6jvx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  =  $config['database']['table_prefix'] . '_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('FORCE_SSL_LOGIN', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
